
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import IndexPage from "./Components/IndexPage";
import {Outlet, Route, Routes} from "react-router-dom";
import CatalogPage from "./Components/CatalogPage";
import About from "./Components/About";
import Delivery from "./Components/Delivery";
import BasketPage from "./Components/BasketPage";
import PersonPage from "./Components/PersonPage";


function App() {
    return (
        <div>
            <Routes>
                <Route path={'/'} element={<Layout/>}>
                    <Route index element={<IndexPage/>}/>
                    <Route path={'catalog'} element={<CatalogPage/>}/>
                    <Route path={'about'} element={<About/>}/>
                    <Route path={'delivery'} element={<Delivery/>}/>
                    <Route path={'basket'} element={<BasketPage/>}/>
                    <Route path={'person'} element={<PersonPage/>}/>

                </Route>
            </Routes>

            <div>

            </div>

        </div>
    );
}

function Layout() {
    return (
        <div >
            <Header/>
            <main >
                <Outlet/>
            </main>
            <Footer/>
        </div>
    )
}


export default App;

