import {Col, Container, Image, Nav, Row} from "react-bootstrap";
import AuthPage from "./AuthPage";
import {Link} from "react-router-dom";


function Header(){
    return(
        <Container className='my-4'>
            <Row>
                <Col md='3' className='text-info d-flex'>
                    <Image
                        src='img/logo.png'
                        width='57'
                        height='40'
                    />
                    <Link className="nav-link" to='/'><h5>AIRSHOP</h5></Link>
                </Col>
                <Col md='6' >
                    <Nav className="justify-content-end" activeKey="/home">
                        <Link className="nav-link" to='/'>Главная</Link>
                        <Link className="nav-link" to='catalog'>Каталог</Link>
                        <Link className="nav-link" to='delivery'>Доставка и оплата</Link>
                        <Link className="nav-link" to='about'>О проекте</Link>

                    </Nav>
                </Col>
                {/*<Col md='1'>*/}


                {/*</Col>*/}
                <Col md='3' className='p-0'>
                    <button type="button" className="btn btn-outline-info " >
                        <Link className="nav-link p-0" to='basket'>
                        <Image src='img/basket.png'/>
                        </Link>
                    </button>
                    <button type="button" className="btn btn-outline-info">
                        <Link className="nav-link p-0" to='person'>
                            <Image src='img/user.png'/>
                        </Link>
                    </button>
                    <AuthPage />
                </Col>
            </Row>
        </Container>
    )

}
export default Header;