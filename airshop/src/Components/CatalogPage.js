import {Container} from "react-bootstrap";
// import CardList from './CardList';
import GoodsList from "../Redux/container/GoodsList";


function CatalogPage(){

    return(
        <Container>
            <h2 className='display-5 text-center'>Каталог</h2>
            <GoodsList/>
        </Container>
    )
}
export default CatalogPage;