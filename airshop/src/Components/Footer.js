import {Col, Container, Navbar, Image, Row, Nav} from "react-bootstrap";
import {Link} from "react-router-dom";

function Footer(){
    return(
        <Container className='text-info'>
            <footer className='border mt-5 mb-5'>
              <Row className='m-2'>
                  <Col className='m-auto'>
                      <Navbar bg='light'>
                          <Container >
                              <Navbar.Brand href="/" className='d-flex'>
                                  <Image
                                      src='img/logo.png'
                                      width="114"
                                      height="80"
                                      className="d-inline-block align-top"
                                      alt="AIRSHOP"
                                  />
                                  <h3 className='display-6 text-info m-auto'>ВОЗДУХ ДЛЯ ТЕБЯ</h3>
                              </Navbar.Brand>
                          </Container>
                      </Navbar>
                  </Col>
                  <Col>
                      <h5>Основное</h5>
                      <Nav className='flex-column' activeKey="/home">
                          <Link className="nav-link" to='/'>Главная</Link>
                          <Link className="nav-link" to='catalog'>Каталог</Link>
                          <Link className="nav-link" to='delivery'>Доставка и оплата</Link>
                          <Link className="nav-link" to='about'>О проекте</Link>

                      </Nav>
                  </Col>
                  <Col>
                      <h5>Полезные ссылки</h5>
                      <Nav className='flex-column' activeKey="/home">
                          <Nav.Link href="https://www.google.ru/" target='_blank'>Поиск в Гугл</Nav.Link>
                          <Nav.Link href="https://www.yandex.ru/" target='_blank'>Поиск в Яндекс</Nav.Link>
                          <Nav.Link href="https://www.mail.ru/" target='_blank'>Поиск в Маил ру</Nav.Link>
                      </Nav>
                  </Col>
              </Row>
            </footer>
        </Container>
    )
}
export default Footer;