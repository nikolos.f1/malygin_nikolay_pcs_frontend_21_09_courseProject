import {Carousel, Container} from "react-bootstrap";
import Users from "../Users/Users";
import GoodsList from "../Redux/container/GoodsList";

function IndexPage(){
    return(
        <Container>
            <div className='border'>
                <h3 className='display-5 text-center text-info'>Честный магазин по продаже воздуха</h3>
            </div>
            <Carousel>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="https://avatars.mds.yandex.net/get-ynews/2758773/b21dd799820c1007da4185f78c424ff0/800x400"
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <h2>Высокое качество</h2>
                        <h3 >Только в нашем магазине вы можете самый качественный в мире воздух</h3>
                    </Carousel.Caption>
                </Carousel.Item>

                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="https://rozvytok.top/wp-content/uploads/2018/10/1416904125_schastye.jpg"
                        alt="Second slide"
                    />
                    <Carousel.Caption >
                            <h2 className="shadow-lg text-dark">
                                Наш воздух доступен каждому
                            </h2>
                    </Carousel.Caption>
                </Carousel.Item>

                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="https://constructorus.ru/wp-content/uploads/2019/01/img_por_que_es_importante_respirar_por_la_nariz_32401_orig.jpg"
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <h3 className='shadow-lg text-dark'>Состав подобраный специально для вас</h3>

                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
            <Container>
                <h2 className='display-5 text-center'>Каталог</h2>
                <GoodsList/>
            </Container>
            <Users/>
        </Container>
    )
}
export default IndexPage;
