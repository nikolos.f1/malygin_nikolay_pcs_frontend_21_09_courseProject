import {Container} from "react-bootstrap";
import React from "react";

function About(){
    return(
        <Container className='text-center'>
            <h2 className='display-3'>О проекте</h2>
            <p className='lead'> Проект для итоговой аттестации слушателя курса <em>«Основы Frontend- разработки (ПЦС)»</em><br/>
            <strong> Малыгина Николая Сергеевича</strong>
            </p>

        </Container>

    );
}

export default About;