
import React from 'react';
import UsersCardList from "./UsersCardList";

export default class Users extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: []
        }
    }

    componentDidMount() {
        fetch('https://reqres.in/api/users?per_page=5')
            .then((response) => response.json())
            .then((result) => {
                this.setState({users: result.data});
            })
    }

    renderUsers() {
        if (this.state.users.length) {
            return <UsersCardList userList={this.state.users}/>
        }

        return <div>Загрузка...</div>
    }

    render() {
        return (
            <div>
                <h1 className='text-info text-center'>Наши постоянные клиенты</h1>

                {this.renderUsers()}
            </div>
        );
    }
}
