import React from 'react';
import UsersCard from "./UsersCard";

export default class UsersCardList extends React.Component {
    static defaultProps = {
        userList: []
    }
    render() {
        return (
            <div className='d-flex flex-wrap'>
                {
                    this.props.userList.map((user) => {
                        return (
                            <div className='card m-2' key={user.id}>
                                <UsersCard
                                    id={user.id}
                                    email={user.email}
                                    firstName={user['first_name']}
                                    lastName={user['last_name']}
                                    avatar={user['avatar']}/>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}
