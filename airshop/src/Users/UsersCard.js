import React from 'react';
import {Card} from "react-bootstrap";

export default class UsersCard extends React.Component {
    static defaultProps = {
        id: 0,
        email: '',
        firstName: '',
        lastName: '',
        avatar: ' '
    }
    render() {
        const { email, firstName, lastName, avatar } = this.props;
        return (
            <Card  style={{ width: '15rem' }}>
                <Card.Body>
                    <Card.Img variant="top" src={avatar}/>
                    <Card.Title>{firstName} {lastName}</Card.Title>
                    <Card.Text>
                        <a href={`mailto:${email}`}>{email}</a>
                    </Card.Text>
                </Card.Body>
            </Card>
        )

    }
}
