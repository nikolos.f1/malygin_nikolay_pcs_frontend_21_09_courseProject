import React from "react";
import {useSelector} from "react-redux";
import {selectGoods} from "../store/goodsSlice";
import Goods from "../Goods";

function GoodsList() {
    const goods = useSelector(selectGoods)
    return(
        <>
            <div className='d-flex flex-wrap'  >
                {goods.map(item => <Goods
                    key={item.id}
                    cardTitle={item.cardTitle}
                    price={item.price}
                    id={item.id}
                    cardText={item.cardText}
                    img={item.img}
                />)}
            </div>
        </>
    )

}
export default GoodsList