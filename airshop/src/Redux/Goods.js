import {Button, Card} from "react-bootstrap";

function Goods(props){
    return(
        <Card className='text-center m-3 pt-2' style={{ width: '13rem'}}>
            <Card.Img variant="top" src={props.img} />
            <Card.Body >
                <Card.Title >{props.cardTitle}</Card.Title>
                <Card.Text>{props.cardText}</Card.Text>


            </Card.Body>
            <h3 className='text-danger mb-3'>{props.price}</h3>
            <Button variant="primary" key={props.id}>Купить</Button>
        </Card>
        // <div>
        //     <img src={props.img} alt=''/>
        //     <p>{props.cardTitle}</p>
        //     <p>{props.cardText}</p>
        //     <p>{props.price}</p>
        //     <button key={props.id}>Добавить</button>
        // </div>
    )
}
export default Goods